const limitFunctionCallCount = require("../limitFunctionCallCount.cjs");

function cb(){
    return "Hi";
}

let res = limitFunctionCallCount(cb, 3);
console.log(res());
console.log(res());
console.log(res());
console.log(res());

let ans = limitFunctionCallCount();
console.log(ans());