const cacheFunction = require('../cacheFunction.cjs');

function cb(param){
    if(Array.isArray(param))
        return param.map((x)=>x*x);
    else if(!isNaN(param))
        return param*param;
    else
        return "square not possible";
}

const res = cacheFunction(cb);
console.log(res(7));
console.log(res(5));
console.log(res([7,2]));
console.log(res(5));
console.log(res("Hi"));
