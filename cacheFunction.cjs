function cacheFunction(cb) 
{
    if(typeof cb != 'function')
        return;
        
    let cache = new Map();

    function invoke(...params)
    {
        let result;
        if(cache.has(...params)==false)
        {
            result = cb(...params);
            cache.set(...params,result);
        }
        return cache;
    }
    
    return invoke;
}

module.exports = cacheFunction;