function limitFunctionCallCount(cb, n) {
    if (typeof cb == 'function' && typeof n == 'number') {
        function invoke() {
            if (n-- > 0)
                return cb();
            else
                return null;
        }
        return invoke;
    }
    else
    {
        function nullFunc()
        {
            return null;
        }
        return nullFunc;
    }
        

}

module.exports = limitFunctionCallCount;