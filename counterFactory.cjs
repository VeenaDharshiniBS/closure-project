function counterFactory() {
    let count = 0;
    let counterObj = {
        increment: function () {
            return ++count;
        },
        decrement: function () {
            return --count;
        }
    };
    return counterObj;
}
module.exports = counterFactory;